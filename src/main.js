import Vue from 'vue'
import vuetify from './plugins/vuetify'
import './plugins/base'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './plugins/axios'
import moment from 'moment'

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  axios,
  moment,
  render: h => h(App),
}).$mount('#app')
